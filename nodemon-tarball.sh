#!/bin/sh

version=$(rpm -q --specfile --qf='%{version}\n' nodejs-nodemon.spec | head -n1)
wget https://registry.npmjs.org/nodemon/-/nodemon-$version.tgz
tar -zxf nodemon-$version.tgz
cd package
npm install --production && rm -rf Dockerfile && cd .. && tar -zcf nodemon-v$version-bundled.tar.gz package
